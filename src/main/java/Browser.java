import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class Browser {
    public static WebDriver driver;

    public static void openBrowser(String browserSelection) {
        switch (browserSelection) {
            case "chrome":
                System.setProperty("webdriver.chrome.driver", "/Users/boyanatanasov/Downloads/chromedriver");
                driver = new ChromeDriver();
                driver.manage().window().maximize();
                driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
                ;
                break;
            default:
                throw new RuntimeException("The chosen browser is" + browserSelection);

        }
    }

}


