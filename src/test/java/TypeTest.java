import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.print.DocFlavor;

public class TypeTest {
    private static final By LOC_FORUMS = By.xpath("//div[@id='nav_bkg']//a[@href='/forums/']");
    private static final By LOC_TEXT = By.xpath("//div[@class='navigation']//span[@class='active']");
    private static final By LOC_NUMBER = By.xpath("//*[@id='cat_7_e']/tr[3]/td[4]");


    @BeforeMethod
    public void setUp() {
        Browser.openBrowser("chrome");
        Page.openPage("web");
    }



    @Test
    public void verifyNumber() throws InterruptedException {
        Browser.driver.findElement(LOC_FORUMS).click();
        Browser.driver.findElement(LOC_TEXT).getText();
        String text = Browser.driver.findElement(LOC_NUMBER).getText();
        Assert.assertEquals("1,264",text,"fck");
        System.out.println(text);
        Thread.sleep(5000);
    }


    @AfterMethod
    public void getDown() {
        Browser.driver.quit();
    }
}
